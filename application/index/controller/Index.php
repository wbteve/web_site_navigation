<?php
namespace app\index\controller;
use think\Controller;

class Index extends Base
{
    public function index()
    {
        // 一级导航
        $fenlei = db('fenlei')->order('f_sort desc')->select();
        $this->assign('fenlei',$fenlei);
        // 精品网站
        $jingpin = db('link_jp')->order('l_sort desc')->limit(24)->select();
        $this->assign('jingpin',$jingpin);
        return $this->fetch();
    }
    
    

}
